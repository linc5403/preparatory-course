
# Table of Contents

1.  [git实际操作流程](#org68e6e65)
2.  [注册git服务](#org5208975)
3.  [git工具](#orgdfea272)
    1.  [工具安装](#org5aa300e)
    2.  [安装后的配置](#org2a49bb8)
4.  [git使用场景](#org73364a0)
    1.  [下载别人的仓库，不进行修改和提交](#org27b17d8)
    2.  [将自己的项目存放在服务器侧，并进行跟踪](#org4e55ff0)


<a id="org68e6e65"></a>

# git实际操作流程

![img](./img/abc.png)


<a id="org5208975"></a>

# 注册git服务

多家公司都提供了git服务，例如[github](https://github.com/), [gitlab](https://about.gitlab.com/), [码云](https://gitee.com/)等，首选需要选择一个服务提供商，并在其网站上注册账号，开通服务。
本文使用码云作为示例进行讲解。


<a id="orgdfea272"></a>

# git工具

我们通常使用命令行来使用git，在不同操作系统下命令行都是一致的。


<a id="org5aa300e"></a>

## 工具安装

-   MAC
    直接从AppStore安装Xcode，Xcode集成了Git，不过默认没有安装，你需要运行Xcode，选择菜单“Xcode”->“Preferences”，在弹出窗口中找到“Downloads”，选择“Command Line Tools”，点“Install”就可以完成安装了。
    
    ![img](./img/xcode.jpeg)

-   Windows
    在git官网下载[安装程序](https://git-scm.com/download/win)，然后按照默认选项进行安装。安装完成后，在开始菜单里找到“Git”->“Git Bash”，蹦出一个类似命令行窗口的东西，就说明Git安装成功！
    
    ![img](./img/win-git.jpeg)

-   Linux  
    不同的发行版本请使用不同的包管理器进行安装，[这个网页](https://git-scm.com/download/linux)有详细的介绍和说明。


<a id="org2a49bb8"></a>

## 安装后的配置

    $ git config --global user.name "Your Name"
    $ git config --global user.email "email@example.com"

这两条命令的作用是初始化你这台机器的全局git配置，包含用户名和Email，当你进行着两个配置之后，提交到仓库中的记录就可以看到你指定的用户名和Email地址。

注意 `git config` 命令的 `--global` 参数，用了这个参数，表示你这台机器上所有的Git仓库都会使用这个配置，当然也可以对某个仓库指定不同的用户名和Email地址。


<a id="org73364a0"></a>

# git使用场景


<a id="org27b17d8"></a>

## 下载别人的仓库，不进行修改和提交

这种一般是借鉴和学习别人代码的场景，通常只需要一条命令就可以完成：

    git clone 远端地址


<a id="org4e55ff0"></a>

## 将自己的项目存放在服务器侧，并进行跟踪

![img](./img/changjing2.png)

